
//  CMG
//  Created by Repon Macbook on 5/14/16.

/*
Data Structure.
Those are smaller than already picked doesnot change the answer. 
So we can ignore them. When popping from the stack , check if 
this is the maximum than Pop from the priority_queue
*/

#include<bits/stdc++.h>
using namespace std;
stack<int>st;
priority_queue<int>S;
int main()
{
    int t,n,cs=0;
    scanf("%d",&t);
    while(t-- ) {
        printf("Case %d:\n",++cs);
        scanf("%d",&n);
        for(int i= 0;i<n;i++){
            char ch;
            scanf(" %c",&ch);
            if(ch=='A'){
                int x;
                scanf("%d",&x);
                if(S.size() == 0 || x>=S.top()){
                    S.push(x);
                }
                st.push(x);
            }
            if(ch=='Q') {
                if(S.size()==0) printf("Empty\n");
                else printf("%d\n",S.top());
            }
            if(ch =='R') {
                if(S.size()) {
                    int k = st.top();st.pop();
                    if(S.top() == k ) S.pop();
                }
            }
        }
        while(!st.empty()) st.pop();
        while(!S.empty()) S.pop();
    }
}