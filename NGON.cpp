#include <bits/stdc++.h>
using namespace std;
const int N = 1001 , MOD = 1e9+7;
long long dp[N][N];
int p, n ;
int a[N];

int main()
{
    int t;
    scanf("%d",&t);
    while(t--) {
        scanf("%d",&n);
        for(int i =1;i <= n; i++ ) scanf("%d",&a[i]);
        p = n - 1;
        memset(dp,0,sizeof dp);
        dp[0][0] = 1;
        for(int i = 1; i <= n; i ++ ) {
            dp[i][0] = 1;
            for(int j = 1; j <= n-1; j ++ ) {
                dp[i][j] = (dp[i-1][j] + (dp[i-1][j-1] * a[i] ) )% MOD;
                if(j>1) {
                    long long o = dp[i-1][j-2] * a[i] * (a[i]-1) / 2;
                    dp[i][j] = (dp[i][j] + o) % MOD;
                }
            }
        }
        printf("%lld\n",dp[n][n-1]);
    
    }
}