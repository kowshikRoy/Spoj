#include <bits/stdc++.h>
using namespace std;
const int N = 64;
unsigned long long dp[64][64];


int M;
int vis[N][N][2];
unsigned long long memo[N][N][2];
unsigned long long calc(int n,int k,int f)
{
    if(k > M) return 0;
    if(n==0){
        if(f) return 1;
        return 0;
    }
    if(vis[n][k][f] == M ) return memo[n][k][f];
    auto r = calc(n-1,0,f); // 0
    r += calc(n-1,k+1, f | ((k+1== M) ? 1 : 0));
    vis[n][k][f] = M;
    return memo[n][k][f] = r;
}
int main()
{
    
    for(int i = 1; i < N; i ++ ) dp[i][0] = 1;
    for(int k = 1; k < N ; k ++ ) {
        M = k;
        for(int n = k; n < N; n ++ ){
            dp[n][k] = calc(n,0,0);
        }
    }
    for(int i = 1; i < N ; i ++ ) {
        for(int j = 0; j <= i ; j ++ ) {
            if(j) printf(" ");
            printf("%llu",dp[i][j]);
        }
        printf("\n");
        
    }
}